package com.wdm;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JMemcachecd{
	private boolean stop;
	  //端口号
	  private final int port;
	  //服务器线程
	  private ServerSocket serverSocket;
	  private final Logger logger = Logger.getLogger(JMemcachecd.class.getName());
	  //线程池，线程容量为maxConnection
	  private final ExecutorService executorService;
	  private final Cache<Item> cache;
	  public JMemcachecd(int port, int maxConnection, Cache<Item> cache){
	    if (maxConnection<=0) throw new IllegalArgumentException("支持的最大连接数量必须为正整数");
	    this.port = port;
	    executorService = Executors.newFixedThreadPool(maxConnection);
	    this.cache = cache;
	  }

	  public void start() {
	    try {
	      serverSocket = new ServerSocket(port);
	      logger.info("服务器在端口"+port+"上启动");
	      while (true){
	        try {
	          Socket socket = serverSocket.accept();
	          logger.info("收到"+socket.getLocalAddress()+"的连接");
	          executorService.submit(new SocketHandler(socket, cache));
	        } catch (IOException e) {
	          e.printStackTrace();
	        }
	      }
	    } catch (IOException e) {
	      logger.log(Level.WARNING, "服务器即将关闭...");
	      e.printStackTrace();
	    } finally {
	      executorService.shutdown();
	      shutDown();
	    }
	  }

	  /**
	   * 服务器是否仍在运行
	   * @return
	   */
	  public boolean isRunning() {
	    return !serverSocket.isClosed();
	  }
	  /**
	   * 停止服务器
	   */
	  public void shutDown(){
	    try {
	      if (serverSocket!=null){
	        serverSocket.close();
	      }
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Cache<Item> cache = new Cache<Item>(500);
		JMemcachecd memcached = new JMemcachecd(2022, 20, cache);
		memcached.start();
	}
}
