package com.wdm;

import java.util.Date;

public class Item {
	String key;
	String flags;
	Date expired;
	int length;
	String value;
	boolean sent = false;
	
	public void setSent()
	{
		sent = true;
	}
	
	public boolean hasSent()
	{
		return sent;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(int expired) {
		this.expired = new Date(System.currentTimeMillis() + expired * 1000);;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getValue() {
		// TODO Auto-generated method stub
		return this.value;
	}

	public boolean hasExpired()
	{
		return new Date().after(this.expired);
	}
	
	public boolean hasExpired(int append) {
		return new Date(System.currentTimeMillis() + append * 1000).after(this.expired);
	}
}
