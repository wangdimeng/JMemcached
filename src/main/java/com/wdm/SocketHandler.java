package com.wdm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.logging.Logger;

import com.wdm.command.Command;
import com.wdm.command.CommandFactory;

/**
 * 处理各个客户端的连接 在获得end指令后关闭连接s
 */
public class SocketHandler implements Runnable {

	private static Logger logger = Logger.getLogger(SocketHandler.class.getName());

	private final Socket socket;

	private Cache<Item> cache;

	public SocketHandler(Socket s, Cache<Item> cache) {
		this.socket = s;
		this.cache = cache;
	}

	public void run() {
		try {
			// 获取socket输入流
			final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// 获取socket输出流
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

			CommandFactory commandFactory = CommandFactory.getInstance(cache);
			
			String commandLine;
			while((commandLine = reader.readLine()) != null) {
				logger.info("ip:" + socket.getLocalAddress() + " 指令:" + commandLine);
				final Command command = commandFactory.getCommand(commandLine);
				command.execute(reader, writer);
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.info("关闭来自" + socket.getLocalAddress() + "的连接");
		} finally {
			try {
				if (socket != null) {
					logger.info("关闭来自" + socket.getLocalAddress() + "的连接");
					socket.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}