package com.wdm;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Cache<Item>{
  private Logger logger = Logger.getLogger(Cache.class.getName());
  //利用LinkedHashMap实现LRU
  private LinkedHashMap<String, Item> cache;
  private final int maxSize;
  //负载因子
  private final float DEFAULT_LOAD_FACTOR = 0.75f;
  public Cache(final int maxSize){
    this.maxSize = maxSize;
    //确保cache不会在达到maxSize之后自动扩容
    int capacity = (int) Math.ceil(maxSize / DEFAULT_LOAD_FACTOR) + 1;

    this.cache = new LinkedHashMap<String, Item>(capacity, DEFAULT_LOAD_FACTOR, true){
      @Override
      protected boolean removeEldestEntry(Map.Entry<String,Item> eldest) {
        if (size() > maxSize){
          logger.info("缓存数量已经达到上限，会删除最近最少使用的条目");
        }
        return size() > maxSize;
      }
    };
    
    //实现同步访问
    Collections.synchronizedMap(cache);
  }

  public synchronized boolean isFull(){
    return cache.size() >= maxSize;
  }

  public Item get(String key) {
    Item item = (Item) cache.get(key);

//    if (item == null){
//      logger.info("缓存中key:" + key + "不存在");
//      return null;
//    }else if(item!=null && item.isExpired()){ //如果缓存过期则删除并返回null
//      logger.info("从缓存中读取key:" + key + " value:" + item.getValue() + "已经失效");
//      cache.remove(key);
//      return null;
//    }

//    logger.info("从缓存中读取key:" + key + " value:" + item.getValue() + " 剩余有效时间" + item.remainTime());
    return item;
  }

  public void set(String key, Item item) {
    logger.info("向缓存中写入key:" + key + "");
    cache.put(key, item);
  }

  
  public Item delete(String key) {
    logger.info("从缓存中删除key:" + key);
    return cache.remove(key);
  }

  
  public int size(){
    return cache.size();
  }

  public int capacity() {
    return maxSize;
  }

  public Iterator<Map.Entry<String, Item>> iterator() {
    return cache.entrySet().iterator();
  }
}
