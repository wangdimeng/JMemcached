package com.wdm.command;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import com.wdm.Cache;
import com.wdm.Item;

public class GetCommand implements Command {
	
	String key;
	Cache<Item> cache;

	public GetCommand(String commandLine, Cache<Item> memcache) {
		String[] params = commandLine.split(" ");
		key = params[1];
		cache = memcache;
	}

	public void execute(Reader reader, Writer writer) {
		// TODO Auto-generated method stub
		try {
			Item item = this.cache.get(this.key);
			if (item != null) {
				if (!item.hasExpired() || item.hasSent() && item.hasExpired(10)) {
					writer.write("VALUE " + item.getKey() + " " + item.getFlags() + " " + item.getLength() + "\r\n");
					writer.write(item.getValue() + "\r\n");
				} else {
					item.setSent();
				}
			}
			
			writer.write("END\r\n");
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CommandType getType() {
		// TODO Auto-generated method stub
		return null;
	}
}
