package com.wdm.command;

import com.wdm.*;

/** * 指令工厂 单一实例 */
public class CommandFactory {
	private static CommandFactory commandFactory;
	private static Cache<Item> memcache;

	private CommandFactory() {
	}

	public static CommandFactory getInstance(Cache<Item> cache) {
		if (commandFactory == null) {
			commandFactory = new CommandFactory();
			memcache = cache;
		}
		return commandFactory;
	}

	/** * 根据指令的类型获取Command * @param commandLine * @return */
	public Command getCommand(String commandLine) {
		if (commandLine.matches("^set .*$") || commandLine.matches("^add .*$")) {
			return new SetCommand(commandLine, memcache);
		} else if (commandLine.matches("^get .*$")) {
			return new GetCommand(commandLine, memcache);
		} else if (commandLine.matches("^delete .*$")) {
			return new DeleteCommand(commandLine, memcache);
		} else if (commandLine.matches("^end$")) {
			return new EndCommand(commandLine);
		} else {
			return new ErrorCommand(commandLine, ErrorCommand.ErrorType.ERROR);
		}
	}
}