package com.wdm.command;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import com.wdm.Cache;
import com.wdm.Item;

/** * ɾ������ָ�� */
public class DeleteCommand implements Command {
	private final String command;
	private final Cache<Item> cache;
	private String key;
	private boolean noReply;

	public DeleteCommand(final String command, final Cache<Item> cache) {
		this.command = command;
		this.cache = cache;
		initCommand();
	}

	private void initCommand() {
		if (this.command.contains("noreply")) {
			noReply = true;
		}
		String[] info = command.split(" ");
		key = info[1];
	}

	public void execute(Reader reader, Writer writer) {
		BufferedWriter bfw = (BufferedWriter) writer;
		Item item = cache.delete(key);
		if (!noReply) {
			try {
				if (item == null) {
					bfw.write("NOT_FOUND\r\n");
				} else {
					bfw.write("DELETED\r\n");
				}
				bfw.flush();
			} catch (IOException e) {
				try {
					bfw.write("ERROR\r\n");
					bfw.flush();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
		}
	}

	public CommandType getType() {
		return CommandType.SEARCH;
	}
}
