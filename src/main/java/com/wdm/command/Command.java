package com.wdm.command;

import java.io.Reader;
import java.io.Writer;

public interface Command {
	/** * 执行指令 * @param reader * @param writer */
	void execute(Reader reader, Writer writer);

	/** * 获取指令的类型 * @return */
	CommandType getType();
}
