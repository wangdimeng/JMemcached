package com.wdm.command;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import com.wdm.Cache;
import com.wdm.Item;

public class SetCommand implements Command {
	Item item = null;
	Cache<Item> cache;
	
	public SetCommand(String commandLine, Cache<Item> memcache) {
		String[] params = commandLine.split(" ");
		item = new Item();
		item.setKey(params[1]);
		item.setFlags(params[2]);
		item.setExpired(Integer.parseInt(params[3]));
		item.setLength(Integer.parseInt(params[4]));
		cache = memcache;
	}

	
	public void execute(Reader reader, Writer writer) {
		try {
			char[] ch = new char[this.item.getLength()];
			reader.read(ch, 0, this.item.getLength());
			reader.read(new char[2], 0, 2);
			this.item.setValue(String.valueOf(ch));
			this.cache.set(item.getKey(), item);
			writer.write("STORED\r\n");
			writer.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CommandType getType() {
		// TODO Auto-generated method stub
		return null;
	}

}
