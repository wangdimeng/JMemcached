package com.wdm;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * 缓存管理器
 * 后台线程
 * 将cache中过期的缓存删除
 */
public class CacheManager implements Runnable {

  private Logger logger = Logger.getLogger(CacheManager.class.getName());

  //缓存
  public Cache<Item> cache;

  public CacheManager(Cache<Item> cache){
    this.cache = cache;
  }


  public void run() {
	  System.out.println("run");
    while (true){
      Iterator<Map.Entry<String, Item>> itemIterator = cache.iterator();
      
      while (itemIterator.hasNext()){
        Map.Entry<String, Item> entry = itemIterator.next();
        Item item = entry.getValue();
        if(item.isExpired()){
          logger.info("key:" + entry.getKey() + " value" + item.getValue() + " 已经过期，从数据库中删除");
          itemIterator.remove();
        }
      }
      
      System.out.println("Cache size:" + cache.size());
      
      try {
        //每隔5秒钟再运行该后台程序
        TimeUnit.SECONDS.sleep(5);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    }
  }
}